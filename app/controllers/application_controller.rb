class ApplicationController < ActionController::Base
  include ApplicationHelper
  before_action :authorize

  protected

  def authorize
    # make sure session is authorized before giving access to certain pages (admin)
    redirect_to login_url, notice: 'Please log in' unless User.find_by(id: session[:user_id])
  end
end
